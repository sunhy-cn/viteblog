---
layout: home
hero:
  name: Sunhy's Blog
  text: 一个基于VitePress的个人博客
  image:
    src: /img/logo.png
    alt: Blog Logo
  actions:
    - theme: brand
      text: 浏览文章
      link: /blog/202211201044
    - theme: alt
      text: 共同学习
      link: /about/
features:
  - icon: 🛠️
    title: 前端前沿
    details: 分享工作中用到的技术及正在学习的新技术
  - icon: 🖖
    title: Bug记录
    details: 记录有意思的bug，欢迎一起讨论
  - icon: ⚡️
    title: 后端学习
    details: 励志早日成为一名全栈！
---