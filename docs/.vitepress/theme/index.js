// 默认样式
import DefaultTheme from 'vitepress/theme'
import './style/var.css'

// 外部组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

// 自定义组件
import demo from '../../src/demoblock/demo.vue'

export default {
  ...DefaultTheme,
  enhanceApp({app}) {
    app.use(ElementPlus)
    app.component('demo', demo)
  }
}