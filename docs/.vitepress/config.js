import { defineConfig } from 'vitepress'
import demoblock from '../src/demoblock'

export default defineConfig({
  base: '/viteblog/',
  outDir: './build',
  title: 'Sunhy`s Blog',
  description: '基于VitePress的个人博客',
  markdown: {
    headers: {
      level: [0, 0]
    },
    config: (md) => {
      md.use(demoblock)
    }
  },
  themeConfig: {
    logo: '/img/logo.png',
    siteTitle: 'Sunhy`s Blog',
    cleanUrls: 'with-subfolders',
    nav: [
      { text: '首页', link: '/' },
      { text: '博文', link: '/blog/202211201044' },
      { text: '关于', link: '/about/' }
    ],
    algolia: {
      appId: '',
      apiKey: '',
      indexName: ''
    },
    socialLinks: [
      { icon: 'github', link: 'https://gitee.com/sunhy-cn/viteblog' }
    ],
    sidebar: sidebarGuide(),
    editLink: {
      pattern: 'https://gitee.com/sunhy-cn/viteblog/tree/master/docs/:path',
      text: 'Edit this page on Gitee'
    },
    lastUpdated: true,
    footer: {
      message: '记录一些学习笔记和收获心得',
      copyright: 'Released under the MIT License.Made with ❤️ by Sunhy',
    }
  }
})

function sidebarGuide() {
  return [
    {
      text: '前端技术',
      collapsible: false,
      items: [
        { text: '开始使用vitepress', link: '/blog/202211201044' },
        { text: '从零开始搭建一个组件库', link: '/blog/202211191756' }
      ]
    }
  ]
}
